﻿using UnityEngine;
using System.Collections;

public class LevelComplete : MonoBehaviour {
	//variable que revisa si el nivel se completo
	int aux = 0;

	public int lvl;
	public GUISkin _theSkin;

	void OnGUI(){
		GUI.skin = _theSkin;
		//si el nivel se completo muestra los botenes para terminar el nivel
		if (aux == 1) {
			//Si es el nivel tres coloca un boton que lleva al menu
			if (lvl == 3) {
				if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 2 + 25, 100, 50), "Fin del juego")) {
					Application.LoadLevel ("Menu");
				}
			} else {
				//Si son el nivel 1 o 2 coloca un boton que lleva al siquiente nivel
				if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 2 + 25, 100, 50), "Siguiente Nivel")) {
					if (lvl == 1) {
						Application.LoadLevel ("Nivel 2");
					}
					if (lvl == 2) {
						Application.LoadLevel ("Nivel 3");
					}
				}
			}
		
			//Boton que lleva al menu al finalizar el nivel 1 o 2
			if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 50), "Menu")) {
				Application.LoadLevel ("Menu");
			}
		}
	}

	//Metodo que se manda a llamar cuando el nivel es completado
	public void Complete(){
		aux++;
	}
}
