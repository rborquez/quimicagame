﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GM : MonoBehaviour {

	//Almacena las preguntas con la respuesta correcta
	int[,] questions = new int[10,2];
	//variable que indica cuantas preguntas a contestado el jugador
	int j = 0;

	//variables que almacena las preguntas que ya fueron usadas
	List<int> usedValues = new List<int>();

	//variable que indica la pregunta que se esta mostrando
	int q;

	//almacena el score dle jugador
	int score;

	//Variables que guardan los sprites de las preguntas y las respuestas
	public Sprite spriteq1;
	public Sprite spriteq1a1;
	public Sprite spriteq1a2;
	public Sprite spriteq1a3;

	public Sprite spriteq2;
	public Sprite spriteq2a1;
	public Sprite spriteq2a2;
	public Sprite spriteq2a3;

	public Sprite spriteq3;
	public Sprite spriteq3a1;
	public Sprite spriteq3a2;
	public Sprite spriteq3a3;

	public Sprite spriteq4;
	public Sprite spriteq4a1;
	public Sprite spriteq4a2;
	public Sprite spriteq4a3;

	public Sprite spriteq5;
	public Sprite spriteq5a1;
	public Sprite spriteq5a2;
	public Sprite spriteq5a3;

	public Sprite spriteq6;
	public Sprite spriteq6a1;
	public Sprite spriteq6a2;
	public Sprite spriteq6a3;

	public Sprite spriteq7;
	public Sprite spriteq7a1;
	public Sprite spriteq7a2;
	public Sprite spriteq7a3;

	public Sprite spriteq8;
	public Sprite spriteq8a1;
	public Sprite spriteq8a2;
	public Sprite spriteq8a3;

	public Sprite spriteq9;
	public Sprite spriteq9a1;
	public Sprite spriteq9a2;
	public Sprite spriteq9a3;

	public Sprite spriteq10;
	public Sprite spriteq10a1;
	public Sprite spriteq10a2;
	public Sprite spriteq10a3;


	void Start(){
		//inicializa el arreglo que almacenara las preguntas
		for (int i = 0; i < 10; i++) {
			questions [i,0] = i+1; 
		}
		//almacena la respuesta correcta en las preguntas
		questions [0,1] = 1;
		questions [1,1] = 2;
		questions [2,1] = 3;
		questions [3,1] = 3;
		questions [4,1] = 3;
		questions [5,1] = 1;
		questions [6,1] = 2;
		questions [7,1] = 1;
		questions [8,1] = 3;
		questions [9,1] = 1;

		//selecciona una pregunta al azar
		randomQuestion ();
		//toma el valor del score que tiene el jugador
		score = PlayerPrefs.GetInt ("score");
	}

	void Update(){
		//Si es necesario actualiza el score del jugador 
		GameObject.Find ("Score").GetComponent<Text> ().text = score.ToString();
	}

	//revisa si la respuesta es correcta, se manda a llamar en los botones
	public void Check(int ans){
		//compara la respuesta con la respuesta correcta de la pregunta
		if (ans == questions [(q-1),1]) {
			Next();
			j++;
			score = score + 1;
			PlayerPrefs.SetInt("score", score);
			PlayerPrefs.Save();
		}
	}

	//cambia a la siguiente pregunta
	void Next(){

		//revisa cuantas preguntas a contestado el jugador
		if (j < 4) {
			//si no a contestado las suficientes manda a llamar el metodo
			randomQuestion();
		}
		//si ya contesto laas suficientes destruye los objetos de las preguntas y muestra los botones para avanzar
		if (j == 4) {
			Destroy(GameObject.Find("Question"));
			Destroy(GameObject.Find("Button1"));
			Destroy(GameObject.Find("Button2"));
			Destroy(GameObject.Find("Button3"));
			GetComponent<LevelComplete>().Complete();	
		}
	}

	//cambia los spirtes de los botones y del componente que muestra la pregunta
	void Change(Sprite q, Sprite a1, Sprite a2, Sprite a3){
		GameObject.Find("Question").GetComponent<SpriteRenderer>().sprite = q;
		GameObject.Find("Button1").GetComponent<Image> ().sprite = a1; 
		GameObject.Find("Button2").GetComponent<Image> ().sprite = a2;
		GameObject.Find("Button3").GetComponent<Image> ().sprite = a3;
	}

	//selecciona una pregunta random
	public void randomQuestion(){
		//selecciona una pregunta random
		q = UniqueRandomInt ();
		//dependiendo de la pregunta seleccionada seran los sprites que se colocaran
		if (q == 1) {
			Change (spriteq1, spriteq1a1, spriteq1a2, spriteq1a3);
		}
		if (q == 2) {
			Change (spriteq2, spriteq2a1, spriteq2a2, spriteq2a3);
		}
		if (q == 3) {
			Change (spriteq3, spriteq3a1, spriteq3a2, spriteq3a3);
		}
		if (q == 4) {
			Change (spriteq4, spriteq4a1, spriteq4a2, spriteq4a3);
		}
		if (q == 5) {
			Change (spriteq5, spriteq5a1, spriteq5a2, spriteq5a3);
		}
		if (q == 6) {
			Change (spriteq6, spriteq6a1, spriteq6a2, spriteq6a3);
		}
		if (q == 7) {
			Change (spriteq7, spriteq7a1, spriteq7a2, spriteq7a3);
		}
		if (q == 8) {
			Change (spriteq8, spriteq8a1, spriteq8a2, spriteq8a3);
		}
		if (q == 9) {
			Change (spriteq9, spriteq9a1, spriteq9a2, spriteq9a3);
		}
		if (q == 10) {
			Change (spriteq10, spriteq10a1, spriteq10a2, spriteq10a3);
		}
	}

	//selecciona la pregunta random que no se haya usado todavia
	public int UniqueRandomInt(){
		//selecciona un valor random
		int val = Random.Range(1, 10);

		//checa si el valor random no a sido usado
		while(usedValues.Contains(val)){
			//si ya fue usado selecciona otro valor
			val = Random.Range(1, 10);
		}

		//agrega el valor seleccionado a los valores usados
		usedValues.Add (val);
		return val;
	}

}

































