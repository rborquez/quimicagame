﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GM2 : MonoBehaviour {

	//Almacena las preguntas con la respuesta correcta
	int[,] questions = new int[10,3];
	//variable que indica cuantas preguntas a contestado el jugador
	int j = 0;

	//variables que almacena las preguntas que ya fueron usadas
	List<int> usedValues = new List<int>();

	//variable que indica la pregunta que se esta mostrando
	int q;

	//almacena el score dle jugador
	int score;

	//Variables que guardan los sprites de las preguntas y las respuestas
	public Sprite spriteq1;
	public Sprite spriteq1a1;
	public Sprite spriteq1a2;
	public Sprite spriteq1a3;
	public Sprite spriteq1a4;
	public Sprite spriteq1a5;
	public Sprite spriteq1a6;

	public Sprite spriteq2;
	public Sprite spriteq2a1;
	public Sprite spriteq2a2;
	public Sprite spriteq2a3;
	public Sprite spriteq2a4;
	public Sprite spriteq2a5;
	public Sprite spriteq2a6;
	
	public Sprite spriteq3;
	public Sprite spriteq3a1;
	public Sprite spriteq3a2;
	public Sprite spriteq3a3;
	public Sprite spriteq3a4;
	public Sprite spriteq3a5;
	public Sprite spriteq3a6;
	
	public Sprite spriteq4;
	public Sprite spriteq4a1;
	public Sprite spriteq4a2;
	public Sprite spriteq4a3;
	public Sprite spriteq4a4;
	public Sprite spriteq4a5;
	public Sprite spriteq4a6;
	
	public Sprite spriteq5;
	public Sprite spriteq5a1;
	public Sprite spriteq5a2;
	public Sprite spriteq5a3;
	public Sprite spriteq5a4;
	public Sprite spriteq5a5;
	public Sprite spriteq5a6;
	
	public Sprite spriteq6;
	public Sprite spriteq6a1;
	public Sprite spriteq6a2;
	public Sprite spriteq6a3;
	public Sprite spriteq6a4;
	public Sprite spriteq6a5;
	public Sprite spriteq6a6;
	
	public Sprite spriteq7;
	public Sprite spriteq7a1;
	public Sprite spriteq7a2;
	public Sprite spriteq7a3;
	public Sprite spriteq7a4;
	public Sprite spriteq7a5;
	public Sprite spriteq7a6;
	
	public Sprite spriteq8;
	public Sprite spriteq8a1;
	public Sprite spriteq8a2;
	public Sprite spriteq8a3;
	public Sprite spriteq8a4;
	public Sprite spriteq8a5;
	public Sprite spriteq8a6;
	
	public Sprite spriteq9;
	public Sprite spriteq9a1;
	public Sprite spriteq9a2;
	public Sprite spriteq9a3;
	public Sprite spriteq9a4;
	public Sprite spriteq9a5;
	public Sprite spriteq9a6;
	
	public Sprite spriteq10;
	public Sprite spriteq10a1;
	public Sprite spriteq10a2;
	public Sprite spriteq10a3;
	public Sprite spriteq10a4;
	public Sprite spriteq10a5;
	public Sprite spriteq10a6;

	//almacena la primera respuesta que da el jugador
	int ans1 = 0;
	//almacena la segunda respuesta que da el jugador
	int ans2 = 0;
	//ve cuantas respuestas tiene el jugador seleccionadas
	int aux = 0;
	
	void Start(){
		//inicializa el arreglo que almacenara las preguntas
		for (int i = 0; i < 10; i++) {
			questions [i,0] = i+1; 
		}
		//almacena la respuesta correcta en las preguntas
		questions [0,1] = 1;
		questions [0,2] = 6;

		questions [1,1] = 2;
		questions [1,2] = 4;

		questions [2,1] = 2;
		questions [2,2] = 3;

		questions [3,1] = 2;
		questions [3,2] = 5;

		questions [4,1] = 3;
		questions [4,2] = 5;

		questions [5,1] = 1;
		questions [5,2] = 2;

		questions [6,1] = 2;
		questions [6,2] = 4;

		questions [7,1] = 4;
		questions [7,2] = 5;

		questions [8,1] = 1;
		questions [8,2] = 4;

		questions [9,1] = 1;
		questions [9,2] = 2;

		//toma el valor del score que tiene el jugador
		score = PlayerPrefs.GetInt ("score");
		//selecciona una pregunta al azar
		randomQuestion ();
	}

	void Update(){
		//Si es necesario actualiza el score del jugador 
		GameObject.Find ("Score").GetComponent<Text> ().text = score.ToString();
	}

	//checa las respuestas del jugador
	public void Check(int ans){
		//descativa la respuesta que fue seleccionada
		Enable (ans);
		//revisa cuantas respuestas an sido seleccionada
		if (aux == 0) {
			//si no a sido ninguna seleccionada almacena la primera respuesta y aumenta el aux
			ans1 = ans;
			aux++;
		} 
		else {
			//si ya fue una seleccionada
			if (aux == 1) {
				//almacena la segunda respuesta y aumenta la variable auxiliar
				ans2 = ans;
				aux++;
			}
			//si ya lleva dos respuestas seleccionadas
			if (aux == 2) {
				//revisa que las dos respuestas seleccionadas sean las correctas sin importar el orden de seleccion
				if ((ans1 == questions [(q-1), 1] && ans2 == questions [(q-1), 2]) || (ans2 == questions [(q-1), 1] && ans1 == questions [(q-1), 2])) {
					Next ();
					j++;
					score = score + 1;
					PlayerPrefs.SetInt("score",score);
					PlayerPrefs.Save();
				}
				//pone aux en 0 si las respuestas no fueron correctas
				aux = 0;
				//vuelve a activar los botones en casa de que las respuestas fueran incorrectas
				GameObject.Find("Button1").GetComponent<Button> ().interactable = true; 
				GameObject.Find("Button2").GetComponent<Button> ().interactable = true; 
				GameObject.Find("Button3").GetComponent<Button> ().interactable = true; 
				GameObject.Find("Button4").GetComponent<Button> ().interactable = true; 
				GameObject.Find("Button5").GetComponent<Button> ().interactable = true; 
				GameObject.Find("Button6").GetComponent<Button> ().interactable = true; 
			}

		}

	}

	//metodo que desactiva las respuestas que el jugador a seleccionado
	public void Enable(int ans){
		if (ans == 1) {
			GameObject.Find("Button1").GetComponent<Button> ().interactable = false; 
		}
		if (ans == 2) {
			GameObject.Find("Button2").GetComponent<Button> ().interactable = false; 
		}
		if (ans == 3) {
			GameObject.Find("Button3").GetComponent<Button> ().interactable = false; 
		}
		if (ans == 4) {
			GameObject.Find("Button4").GetComponent<Button> ().interactable = false; 
		}
		if (ans == 5) {
			GameObject.Find("Button5").GetComponent<Button> ().interactable = false; 
		}
		if (ans == 6) {
			GameObject.Find("Button6").GetComponent<Button> ().interactable = false; 
		}
	}

	//metodo que cambia a la siguiente pregunta
	void Next(){
		//if que ve cuantas preguntas lleva y cambia a la siguiente
		if (j < 4) {
			randomQuestion();
		}

		//si ya lleva 5 preguntas destruye los componentes y coloca los botones
		if (j == 4) {
			Destroy(GameObject.Find("Question"));
			Destroy(GameObject.Find("Button1"));
			Destroy(GameObject.Find("Button2"));
			Destroy(GameObject.Find("Button3"));
			Destroy(GameObject.Find("Button4"));
			Destroy(GameObject.Find("Button5"));
			Destroy(GameObject.Find("Button6"));
			GetComponent<LevelComplete>().Complete();	
		}
	}

	//cambia los sprites de la pregunta y de las respuestas
	void Change(Sprite q, Sprite a1, Sprite a2, Sprite a3, Sprite a4, Sprite a5, Sprite a6){
		GameObject.Find("Question").GetComponent<SpriteRenderer>().sprite = q;
		GameObject.Find("Button1").GetComponent<Image> ().sprite = a1; 
		GameObject.Find("Button2").GetComponent<Image> ().sprite = a2;
		GameObject.Find("Button3").GetComponent<Image> ().sprite = a3;
		GameObject.Find("Button4").GetComponent<Image> ().sprite = a4; 
		GameObject.Find("Button5").GetComponent<Image> ().sprite = a5;
		GameObject.Find("Button6").GetComponent<Image> ().sprite = a6;
	}

	//selecciona una pregunta random y hace el cambio
	public void randomQuestion(){
		q = UniqueRandomInt ();
		if (q == 1) {
			Change (spriteq1, spriteq1a1, spriteq1a2, spriteq1a3, spriteq1a4, spriteq1a5, spriteq1a6);
		}
		if (q == 2) {
			Change (spriteq2, spriteq2a1, spriteq2a2, spriteq2a3, spriteq2a4, spriteq2a5, spriteq2a6);
		}
		if (q == 3) {
			Change (spriteq3, spriteq3a1, spriteq3a2, spriteq3a3, spriteq3a4, spriteq3a5, spriteq3a6);
		}
		if (q == 4) {
			Change (spriteq4, spriteq4a1, spriteq4a2, spriteq4a3, spriteq4a4, spriteq4a5, spriteq4a6);
		}
		if (q == 5) {
			Change (spriteq5, spriteq5a1, spriteq5a2, spriteq5a3, spriteq5a4, spriteq5a5, spriteq5a6);
		}
		if (q == 6) {
			Change (spriteq6, spriteq6a1, spriteq6a2, spriteq6a3, spriteq6a4, spriteq6a5, spriteq6a6);
		}
		if (q == 7) {
			Change (spriteq7, spriteq7a1, spriteq7a2, spriteq7a3, spriteq7a4, spriteq7a5, spriteq7a6);
		}
		if (q == 8) {
			Change (spriteq8, spriteq8a1, spriteq8a2, spriteq8a3, spriteq8a4, spriteq8a5, spriteq8a6);
		}
		if (q == 9) {
			Change (spriteq9, spriteq9a1, spriteq9a2, spriteq9a3, spriteq9a4, spriteq9a5, spriteq9a6);
		}
		if (q == 10) {
			Change (spriteq10, spriteq10a1, spriteq10a2, spriteq10a3, spriteq10a4, spriteq10a5, spriteq10a6);
		}
	}

	//selecciona una pregunta random que no se haya usado
	public int UniqueRandomInt(){
		int val = Random.Range(1, 10);
		
		while(usedValues.Contains(val)){
			val = Random.Range(1, 10);
		}
		
		usedValues.Add (val);
		return val;
	}

}


