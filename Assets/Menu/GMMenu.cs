﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GMMenu : MonoBehaviour {

	int score;

	void Start(){
		score = PlayerPrefs.GetInt ("score");
		//Desbloquea el nivel 3 si el jugador tiene un score mayor que 15
		if (score < 15) {
			GameObject.Find ("Button3").GetComponent<Button> ().interactable = false; 
		} else{
			GameObject.Find("Button3").GetComponent<Button> ().interactable = true; 
		}

	}

	void Update(){
		GameObject.Find ("Score").GetComponent<Text> ().text = score.ToString();
	}

	//Dependiendo de que reciba es al nivel que va a cargar. La funcion esta siendo llamada en los botones del menu.
	public void GoToLevel(int lvl){
		if (lvl == 1) {
			Application.LoadLevel ("Nivel1");
		}
		if (lvl == 2) {
			Application.LoadLevel ("Nivel 2");
		}
		if (lvl == 3) {
			Application.LoadLevel ("Nivel 3");
		}
	}
}
